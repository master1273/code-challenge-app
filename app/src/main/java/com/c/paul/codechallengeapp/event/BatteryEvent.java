package com.c.paul.codechallengeapp.event;

/**
 * Created by Paul Chernenko on 08.05.2017.
 */

public class BatteryEvent {
    private int batteryLevel;
    private boolean charging;

    public int getBatteryLevel() {
        return batteryLevel;
    }

    public void setBatteryLevel(int batteryLevel) {
        this.batteryLevel = batteryLevel;
    }

    public boolean isCharging() {
        return charging;
    }

    public void setCharging(boolean charging) {
        this.charging = charging;
    }
}
