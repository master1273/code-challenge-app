package com.c.paul.codechallengeapp.service;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.IBinder;

import com.c.paul.codechallengeapp.event.LocationEvent;
import com.c.paul.codechallengeapp.provider.LocationProvider;
import com.c.paul.codechallengeapp.util.ContextProvider;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by Paul Chernenko on 08.05.2017.
 */

public class LocationService extends Service {

    private int interval = 1000;
    private long timestamp = System.currentTimeMillis();

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        LocationProvider.getInstance().removeLocationUpdates();
        super.onDestroy();
    }

    @Override
    public void onCreate() {
        LocationProvider locationProvider = LocationProvider.getInstance();

        locationProvider.configure(ContextProvider.getAppContext());

        locationProvider.setCallback(new LocationProvider.Callback() {
            @Override
            public void onLocationChanged(Location location) {
                if (timestamp + interval <= System.currentTimeMillis()) {
                    LocationEvent locationEvent = new LocationEvent();

                    locationEvent.setLatitude(location.getLatitude());
                    locationEvent.setLongitude(location.getLongitude());

                    EventBus.getDefault().post(locationEvent);

                    timestamp = System.currentTimeMillis();
                }
            }
        });

        locationProvider.startLocationUpdates();

        super.onCreate();
    }
}
