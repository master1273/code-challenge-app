package com.c.paul.codechallengeapp.util;

import android.content.Context;

/**
 * Created by Paul Chernenko on 08.05.2017.
 */

public class ContextProvider {

    private static Context appContext;

    public static void init(Context context) {
        appContext = context.getApplicationContext();
    }

    public static Context getAppContext() {
        return appContext;
    }
}