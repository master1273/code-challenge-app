package com.c.paul.codechallengeapp.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;

import com.c.paul.codechallengeapp.R;


/**
 * Created by Paul Chernenko on 09.05.2017.
 */
public class FontTextView extends android.support.v7.widget.AppCompatTextView {

    public FontTextView(Context context) {
        super(context);
        init(context, null, 0);
    }
    public FontTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }
    public FontTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs, defStyle);
    }
    private void init(Context context, AttributeSet attrs, int defStyle) {
        // Load attributes
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.TextViewPlusFont, 0, 0);
        try {
            String fontInAssets = ta.getString(R.styleable.TextViewPlusFont_customFont);
            setTypeface(Typefaces.get(context, "fonts/"+ fontInAssets));
        } finally {
            ta.recycle();
        }
    }
}
