package com.c.paul.codechallengeapp.web;

import android.content.Context;
import android.webkit.JavascriptInterface;

/**
 * Created by Paul Chernenko on 08.05.2017.
 */

public class JavaScriptInterface {

    Context mContext;

    /* Instantiate the interface and set the context */
    public JavaScriptInterface(Context c) {
        mContext = c;
    }

    @JavascriptInterface
    public void changeText() {

    }
}
