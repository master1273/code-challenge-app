package com.c.paul.codechallengeapp.web;

import android.content.Context;
import android.webkit.JavascriptInterface;

import com.c.paul.codechallengeapp.util.Vibration;

/**
 * Created by Paul Chernenko on 09.05.2017.
 */

public class WebApp4Interface {

    Context mContext;

    public WebApp4Interface(Context c) {
        mContext = c;
    }

    @JavascriptInterface
    public void startVibration() {
        if (Vibration.isVibrating()) {
            Vibration.cancel();
        } else {
            Vibration.vibrate();
        }
    }
}