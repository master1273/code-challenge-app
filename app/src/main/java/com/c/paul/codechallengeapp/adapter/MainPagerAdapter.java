package com.c.paul.codechallengeapp.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.c.paul.codechallengeapp.R;
import com.c.paul.codechallengeapp.fragment.WebApp1Fragment;
import com.c.paul.codechallengeapp.fragment.WebApp2Fragment;
import com.c.paul.codechallengeapp.fragment.WebApp3Fragment;
import com.c.paul.codechallengeapp.fragment.WebApp4Fragment;

/**
 * Created by Paul Chernenko on 08.05.2017.
 */

public class MainPagerAdapter extends FragmentPagerAdapter {

    private Context mContext;

    public MainPagerAdapter(FragmentManager fragmentManager, Context context) {
        super(fragmentManager);
        mContext = context;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return WebApp1Fragment.newInstance();
            case 1:
                return WebApp2Fragment.newInstance();
            case 2:
                return WebApp3Fragment.newInstance();
            case 3:
                return WebApp4Fragment.newInstance();
            default:
                throw new IllegalArgumentException("Unknown page position: " + position);
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
            case 1:
            case 2:
            case 3:
                return String.format(mContext.getString(R.string.web_app_title), position + 1);
            default:
                throw new IllegalArgumentException("Unknown page position: " + position);
        }
    }
}
