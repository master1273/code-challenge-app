package com.c.paul.codechallengeapp.provider;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;

import com.c.paul.codechallengeapp.util.ContextProvider;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;

/**
 * Created by Paul Chernenko on 08.05.2017.
 */

public class LocationProvider {

    private static LocationProvider instance = null;
    private static Context context;

    public static final int INTERVAL = 1000;

    private static Location currentLocation;
    private static Callback mCallback;

    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;
    private LocationSettingsRequest.Builder builder;
    private LocationListener locationListener;

    public void setCallback(Callback callback) {
        mCallback = callback;
    }

    private LocationProvider() {

    }

    public static LocationProvider getInstance() {
        if (instance == null) {
            instance = new LocationProvider();
        }

        return instance;
    }

    public void configure(Context ctx) {
        context = ctx;

        if (locationRequest == null)
            locationRequest = createLocationRequest();

        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(context)
                    .addApi(LocationServices.API)
                    .build();

            googleApiClient.registerConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                @Override
                public void onConnected(Bundle bundle) {

                }

                @Override
                public void onConnectionSuspended(int i) {

                }
            });
            googleApiClient.registerConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                @Override
                public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                }
            });
        }

        if (!googleApiClient.isConnected())
            googleApiClient.connect();

        if (builder == null)
            builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
    }

    public PendingResult<LocationSettingsResult> getGPSCheckResult(){
        return LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
    }

    public GoogleApiClient getGoogleApiClient(){
        return googleApiClient;
    }

    private static LocationRequest createLocationRequest() {
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(INTERVAL);
        return locationRequest;
    }

    public void startLocationUpdates(){

        locationListener = new com.google.android.gms.location.LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                currentLocation = location;
                if (mCallback != null)
                    mCallback.onLocationChanged(currentLocation);
            }
        };

        startLocationUpdates(googleApiClient, locationRequest, locationListener);
    }

    private static void startLocationUpdates(GoogleApiClient googleApiClient, LocationRequest locationRequest, com.google.android.gms.location.LocationListener locationListener) {

        if (checkPermission(ContextProvider.getAppContext())) {
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, locationListener);
        }

    }

    public void removeLocationUpdates(){
        LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, locationListener);
    }

    public static boolean checkPermission(final Context context) {
        return ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    public interface Callback{
        void onLocationChanged(Location location);
    }
}