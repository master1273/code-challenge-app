package com.c.paul.codechallengeapp.util;

import android.content.Context;
import android.os.Vibrator;

/**
 * Created by Paul Chernenko on 09.05.2017.
 */

public class Vibration {

    private static Vibrator vibrator;
    private static boolean isVibrating = false;

    private static void init(){
        vibrator = (Vibrator) ContextProvider.getAppContext().getSystemService(Context.VIBRATOR_SERVICE);
    }

    public static void vibrate(){
        if (vibrator == null)
            init();

        if (vibrator.hasVibrator()) {
            long[] pattern = { 0, 200, 0 };

            vibrator.vibrate(pattern, 0);

            isVibrating = true;

        }
    }

    public static void cancel(){
        if (vibrator == null)
            init();

        vibrator.cancel();
        isVibrating = false;
    }

    public static boolean isVibrating(){
        return isVibrating;
    }

}
