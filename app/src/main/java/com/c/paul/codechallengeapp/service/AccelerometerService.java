package com.c.paul.codechallengeapp.service;

import android.app.Service;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;

import com.c.paul.codechallengeapp.event.AccelerometerEvent;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by Paul Chernenko on 08.05.2017.
 */

public class AccelerometerService extends Service implements SensorEventListener {

    private SensorManager manager;

    private long timestamp = System.currentTimeMillis();

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        manager.unregisterListener(this);
    }

    @Override
    public void onCreate() {

        manager = (SensorManager)getSystemService(SENSOR_SERVICE);
        manager.registerListener(this, manager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);

        super.onCreate();
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        int interval = 1000;
        if (timestamp + interval <= System.currentTimeMillis()){

            AccelerometerEvent accelerometrEvent = new AccelerometerEvent();
            accelerometrEvent.setX(sensorEvent.values[0]);
            accelerometrEvent.setY(sensorEvent.values[1]);
            accelerometrEvent.setZ(sensorEvent.values[2]);

            EventBus.getDefault().post(accelerometrEvent);

            timestamp = System.currentTimeMillis();
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}

