package com.c.paul.codechallengeapp.application;

import android.app.Application;

import com.c.paul.codechallengeapp.provider.LocationProvider;
import com.c.paul.codechallengeapp.util.ContextProvider;

/**
 * Created by Paul Chernenko on 08.05.2017.
 */

public class CodeChallengeApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        ContextProvider.init(this);

        LocationProvider.getInstance().configure(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
