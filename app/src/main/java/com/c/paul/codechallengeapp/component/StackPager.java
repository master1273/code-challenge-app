package com.c.paul.codechallengeapp.component;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.util.AttributeSet;

import com.bartoszlipinski.flippablestackview.FlippableStackView;

/**
 * Created by Paul Chernenko on 09.05.2017.
 */

public class StackPager extends FlippableStackView {

    private int position = 0;

    public StackPager(Context context) {
        super(context);
    }

    public StackPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void setAdapter(PagerAdapter adapter) {
        super.setAdapter(adapter);
        setCurrentItem(position);
    }

    public void resetAdapter(){
        if (getAdapter() != null){
            position = getCurrentItem();
            setAdapter(getAdapter());
        }
    }
}
