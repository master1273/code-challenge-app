package com.c.paul.codechallengeapp.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.c.paul.codechallengeapp.R;
import com.c.paul.codechallengeapp.activity.MainActivity;
import com.c.paul.codechallengeapp.component.FontTextView;
import com.c.paul.codechallengeapp.util.ContextProvider;
import com.c.paul.codechallengeapp.util.ResourceUtil;
import com.c.paul.codechallengeapp.web.WebApp4Interface;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Paul Chernenko on 08.05.2017.
 */

public class WebApp4Fragment extends Fragment {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.title)
    FontTextView title;

    @BindView(R.id.web_view)
    WebView webView;

    @OnClick(R.id.toolbar)
    void onFragmentClick(){
        ((MainActivity)getActivity()).onPagerClick();
    }

    public static WebApp4Fragment newInstance() {
        return new WebApp4Fragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_web_app, container, false);
        ButterKnife.bind(this, view);

        title.setText(String.format(ContextProvider.getAppContext().getResources().getString(R.string.web_app_title), 4));
        toolbar.setBackgroundColor(ResourceUtil.getColor(getActivity(), R.color.web_app4));

        webView.setWebViewClient(new WebViewClient());

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        webView.addJavascriptInterface(new WebApp4Interface(getActivity()), "Android");

        webView.loadUrl(String.format("file:///android_asset/%s", ContextProvider.getAppContext().getResources().getString(R.string.web_app4)));

        return view;
    }
}
