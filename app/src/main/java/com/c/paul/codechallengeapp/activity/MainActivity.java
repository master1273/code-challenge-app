package com.c.paul.codechallengeapp.activity;

import android.Manifest;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.bartoszlipinski.flippablestackview.StackPageTransformer;
import com.c.paul.codechallengeapp.R;
import com.c.paul.codechallengeapp.adapter.MainPagerAdapter;
import com.c.paul.codechallengeapp.component.StackPager;
import com.c.paul.codechallengeapp.provider.LocationProvider;
import com.c.paul.codechallengeapp.service.AccelerometerService;
import com.c.paul.codechallengeapp.service.BatteryService;
import com.c.paul.codechallengeapp.service.LocationService;
import com.c.paul.codechallengeapp.util.ContextProvider;
import com.c.paul.codechallengeapp.util.Vibration;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;


import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    private static final int PERMISSIONS_REQUEST = 9090;
    private static final int REQUEST_CHECK_SETTINGS = 2001;

    private boolean doubleBackToExitPressedOnce;
    private Handler backHandler = new Handler();
    private final Runnable backRunnable = new Runnable() {
        @Override
        public void run() {
            doubleBackToExitPressedOnce = false;
        }
    };

    private float currentPagerScale = 1.0f;

    @BindView(R.id.pager)
    StackPager pager;

    public void onPagerClick(){
        currentPagerScale = currentPagerScale == 1.0f ? 1.25f : 1.0f;
        pager.setScaleX(currentPagerScale == 1.0 ? currentPagerScale : currentPagerScale * 1.06f);
        pager.setScaleY(currentPagerScale);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        pager.initStack(4, StackPageTransformer.Orientation.HORIZONTAL,
                0.8f,
                0.4f,
                0.4f,
                StackPageTransformer.Gravity.CENTER);

        pager.setAdapter(new MainPagerAdapter(getSupportFragmentManager(), this));

        //Starting accelerometer service
        startAccelerometerService();

        //Starting location service
        if (requestPermissions()) {
            checkGPS();
        }

        //Starting battery listener
        startBatteryService();

    }

    private void startAccelerometerService() {
        Intent intent = new Intent(new Intent(getBaseContext(), AccelerometerService.class));
        startService(intent);
    }

    private void stopAccelerometerService() {
        stopService(new Intent(getBaseContext(), AccelerometerService.class));
    }

    private void startLocationService() {
        Intent intent = new Intent(new Intent(getBaseContext(), LocationService.class));
        startService(intent);
    }

    private void stopLocationService() {
        stopService(new Intent(getBaseContext(), LocationService.class));
    }

    private void startBatteryService() {
        Intent intent = new Intent(new Intent(getBaseContext(), BatteryService.class));
        startService(intent);
    }

    private void stopBatteryService() {
        stopService(new Intent(getBaseContext(), BatteryService.class));
    }

    private boolean requestPermissions(){
        boolean result = true;

        if (Build.VERSION.SDK_INT < 23)
            return true;

        ArrayList<String> permissions = new ArrayList<>();

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        }

        if (!permissions.isEmpty()){
            result = false;
            ActivityCompat.requestPermissions(this,
                    permissions.toArray(new String[permissions.size()]),
                    PERMISSIONS_REQUEST);
        }

        return result;
    }

    private void checkGPS(){

        PendingResult<LocationSettingsResult> result = LocationProvider.getInstance().getGPSCheckResult();
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult result) {
                final Status status = result.getStatus();
                //final LocationSettingsStates locationSettingsStates= result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        startLocationService();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(
                                    MainActivity.this,
                                    REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        Toast.makeText(ContextProvider.getAppContext(), "Location settings are inadequate, and cannot be fixed here.", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    checkGPS();
                }
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (Vibration.isVibrating())
            Vibration.cancel();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopBatteryService();
        stopAccelerometerService();
        stopLocationService();
    }

    @Override
    public void onBackPressed() {

        if (doubleBackToExitPressedOnce) {
            finish();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(ContextProvider.getAppContext(), R.string.back_message, Toast.LENGTH_SHORT).show();

        backHandler.postDelayed(backRunnable, 2000);
    }

}
