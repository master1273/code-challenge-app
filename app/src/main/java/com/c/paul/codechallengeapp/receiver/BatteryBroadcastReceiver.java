package com.c.paul.codechallengeapp.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.BatteryManager;

import com.c.paul.codechallengeapp.event.BatteryEvent;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by Paul Chernenko on 08.05.2017.
 */

public class BatteryBroadcastReceiver extends BroadcastReceiver{

    int deviceStatus;

    @Override
    public void onReceive(Context context, Intent intent) {

        context.unregisterReceiver(this);

        deviceStatus = intent.getIntExtra(BatteryManager.EXTRA_STATUS,-1);
        int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
        int batteryLevel=(int)(((float)level / (float)scale) * 100.0f);

        BatteryEvent batteryEvent = new BatteryEvent();

        batteryEvent.setBatteryLevel(batteryLevel);
        batteryEvent.setCharging(deviceStatus == BatteryManager.BATTERY_STATUS_CHARGING);

        EventBus.getDefault().post(batteryEvent);
    }
}