package com.c.paul.codechallengeapp.service;

import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.IBinder;

import com.c.paul.codechallengeapp.receiver.BatteryBroadcastReceiver;

/**
 * Created by Paul Chernenko on 08.05.2017.
 */

public class BatteryService extends Service {

    private boolean isActive = false;

    private Handler handler = new Handler();
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            registerReceiver(new BatteryBroadcastReceiver(), new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
            if (isActive)
                handler.postDelayed(runnable, 1000);
        }
    };

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isActive = false;
    }

    @Override
    public void onCreate() {
        isActive = true;
        handler.post(runnable);
        super.onCreate();
    }
}
