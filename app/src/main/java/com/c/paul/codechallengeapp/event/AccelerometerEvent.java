package com.c.paul.codechallengeapp.event;

/**
 * Created by Paul Chernenko on 08.05.2017.
 */

public class AccelerometerEvent {

    private float X;
    private float Y;
    private float Z;

    public float getX() {
        return X;
    }

    public void setX(float x) {
        X = x;
    }

    public float getY() {
        return Y;
    }

    public void setY(float y) {
        Y = y;
    }

    public float getZ() {
        return Z;
    }

    public void setZ(float z) {
        Z = z;
    }
}
