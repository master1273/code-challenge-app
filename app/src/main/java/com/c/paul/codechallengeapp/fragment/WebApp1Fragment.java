package com.c.paul.codechallengeapp.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.c.paul.codechallengeapp.R;
import com.c.paul.codechallengeapp.activity.MainActivity;
import com.c.paul.codechallengeapp.component.FontTextView;
import com.c.paul.codechallengeapp.event.AccelerometerEvent;
import com.c.paul.codechallengeapp.util.ContextProvider;
import com.c.paul.codechallengeapp.util.ResourceUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Paul Chernenko on 08.05.2017.
 */

public class WebApp1Fragment extends Fragment {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.title)
    FontTextView title;

    @BindView(R.id.web_view)
    WebView webView;

    @OnClick(R.id.toolbar)
    void onFragmentClick(){
        ((MainActivity)getActivity()).onPagerClick();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(AccelerometerEvent event) {

        String timestamp = DateFormat.getTimeInstance(DateFormat.MEDIUM, Locale.US).format(new Date());
        String x = String.format(Locale.US, "X: %.3f", event.getX());
        String y = String.format(Locale.US, "Y: %.3f", event.getY());
        String z = String.format(Locale.US, "Z: %.3f", event.getZ());


        String jsString = "javascript:setText('" + timestamp + "','" + x + "','" + y + "','" + z + "');";
        webView.loadUrl(jsString);
    }

    public static WebApp1Fragment newInstance() {
        return new WebApp1Fragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_web_app, container, false);
        ButterKnife.bind(this, view);

        title.setText(String.format(ContextProvider.getAppContext().getResources().getString(R.string.web_app_title), 1));
        toolbar.setBackgroundColor(ResourceUtil.getColor(getActivity(), R.color.web_app1));


        webView.setWebViewClient(new WebViewClient());

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        webView.loadUrl(String.format("file:///android_asset/%s", ContextProvider.getAppContext().getResources().getString(R.string.web_app1)));

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }
}

