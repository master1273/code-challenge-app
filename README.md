# README #

### What? ###

* Create an Android app which let’s the user navigate between 4 web-apps. Basically a web-app chooser. 
* The attached animation mock-up & Design Sketch file (free trial: https://www.sketchapp.com/) should be used as guideline / UI assets 
* The web apps are loaded and kept in memory once loaded the first time. 
* The loading time of a web app should be taken into account. I.e. the native UI has to bridge the time until the web view has fully loaded the content.
* The native app sends periodically (every second) defined data to one of 3 of web-apps (via simple javascript interface) respectively: 
  * web-app 1: X, Y, Z axis of the phone accelerometer 
  * web-app 2: Current location (latitude, longitude) on the phone
  * web-app 3: Current phone battery level in %, also info if the phone is charging or not
* Data must still be sent and received while a web-app is in background (after first opening) 
* All these 3 web-apps are a simple HTML pages which show the received data in a log with prepended timestamp in an autoscrolling fullscreen textfield. 
* Feel free to create something visually more appealing/creative if you like. But the importance is in showing the history of the data received via the Android app.
  * web-app 4: The native app receives a command (javascript interface) from the 4th web-app which let’s the phone vibrate while this web-app is in foreground. The command should be triggered by a simple HTML button.

### Hints ###

* You could use Codepen (https://codepen.io/) for hosting it. You can find some nice web frontend examples here as well to feed the received data with (to replace the simple log textview). 
* Use the Android framework (System Back Button, Activities, Fragments, Views, etc. ) in a reasonable way.
* Use existing UI components and customize them or completely custom UI elements if absolutely necessary. 
* Share the code on some private repository and provide a quick readme describing the project
* Please also justify any 3rd party dependency you use.

### Deadline ###

* Wednesday, May 10th at 8pm (German time, (GMT+1))